import sys
import pika
from gui import file_name, hostname, que_name, routing_key
connection = pika.BlockingConnection(pika.ConnectionParameters(hostname))
channel = connection.channel()
channel.queue_declare(queue=que_name)

fname=file_name
def file_read(fname):
        with open (fname, "r") as myfile:
                data=myfile.readlines()
        return data
message = ''.join(file_read(fname))
#message = ' '.join(sys.argv[1:]) or "Hello World!"
channel.basic_publish(exchange='',
                      routing_key=routing_key,
                      body=message)
print(" [x] Sent %r" % message)
#connection.close()
