import time
import pika
from gui import file_name, hostname, que_name, routing_key

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=hostname))
channel = connection.channel()

channel.queue_declare(queue=que_name)

def callback(ch, method, properties, body):
    print ("Received"% body)
    time.sleep(body.count(b'.'))
#    print ("DOne")
    print(" [x] Done")


channel.basic_consume(
    queue=que_name, on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()    
